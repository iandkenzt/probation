### Installation ###

* Setup Application & Environment:
  1) clone repository
  2) change directory to Application
  3) create virtual environment: virtualenv -p python3.5 env --no-site-packages

* Run Application
  1) activate virtualenv: source your_path/env/bin/activate
  2) install requirements: pip3.5 install -r requirements.txt (position in directory app)
  3) move to folder Application: python3.5 run.py
	
### How to test Application (with Postman) ###

* Registration user:
  1) Method: POST
  2) URL: http://127.0.0.1:5000/register
  3) Input Body form-data key and value:
      * email      = your email
      * name       = your name
      * password   = your password
  4) copy JWT code.

* Login user:
  1) Method: POST
  2) URL: http://127.0.0.1:5000/login
  3) Input Headers key and value:
      * Authorization   = JWT [JWT code]

* Admin get list all user:
  1) Method: GET
  2) URL: http://127.0.0.1:5000/admins