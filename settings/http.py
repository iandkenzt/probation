import routes

from controllers.admin import AdminController
from controllers.user import UserController
from flask import Flask
from flask_jwt import JWT
from helpers import mongo

auth_user = UserController()
auth_admin = AdminController()

_jwt = JWT(authentication_handler=auth_user.authenticate, identity_handler=auth_user.identity)

def create_app(configuration):
    app = Flask(__name__.split(',')[0])
    app.config.from_object(configuration)

    # Register route blueprint
    app.register_blueprint(routes.admin)
    app.register_blueprint(routes.user)

    # # setting jwt
    _jwt.init_app(app)

    # # pymongo
    mongo.init_app(app)

    return app
