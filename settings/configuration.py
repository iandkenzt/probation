import os
from datetime import timedelta

from dotenv import load_dotenv, find_dotenv
from pymongo.read_preferences import ReadPreference

from urllib.parse import quote_plus

# Load environment
load_dotenv(find_dotenv())

# get absolute path static directory in root project
#upload_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'static'))

# logging formatter based on
# https://docs.python.org/2/howto/logging-cookbook.html#an-example-dictionary-based-configuration
logconfiguration = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s",
            'datefmt': "%Y-%m-%d %H:%M:%S"
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'verbose',
            'stream': 'ext://sys.stdout'
        },
        'logstash': {
            'level': 'DEBUG',
            'class': 'logstash.TCPLogstashHandler',
            'host': '35.187.235.8',
            'port': 5044,  # Default value: 5959
            'version': 1,  # Version of logstash event schema. Default value: 0 (for backward compatibility of the library)
            'message_type': 'logstash',  # 'type' field in logstash message. Default value: 'logstash'.
            'fqdn': False,  # Fully qualified domain name. Default value: false.
            'tags': [os.getenv("LOG_TAGS", "managix_api")],  # list of tags. Default: None.
        },
    },
    'loggers': {
        'managix_logger': {
            'propagate': True,
            'handlers': ['logstash'],
            'level': 'DEBUG'
        },
        'create_campaign': {
            'propagate': False,
            'handlers': ['logstash', 'console'],
            'level': 'DEBUG'
        },
        'update_status': {
            'propagate': False,
            'handlers': ['logstash', 'console'],
            'level': 'DEBUG'
        },
        'email_logger': {
            'propagate': False,
            'handlers': ['logstash', 'console'],
            'level': 'DEBUG'
        }
    },
    'root': {
        'level': 'DEBUG',
        'handlers': ['logstash', 'console']
    }
}


class Configuration(object):
    # Mongo configuration
    MONGO_DBNAME = os.getenv("MONGO_DBNAME", "mongojwt")
    MONGO_USERNAME = os.getenv("MONGO_USERNAME", "")
    MONGO_PASSWORD = os.getenv("MONGO_PASSWORD", "")
    MONGO_HOST = os.getenv("MONGO_HOST", "127.0.0.1")
    MONGO_PORT = int(os.getenv("MONGO_PORT", 27017))

    # JWT
    JWT_ALGORITHM = "HS256"
    JWT_SECRET_KEY = "mongojwt"
    JWT_AUTH_URL_RULE = None
    JWT_EXPIRATION_DELTA = timedelta(days=7)  # token expired in 1 weeks
