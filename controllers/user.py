from bson.objectid import ObjectId
from helpers.user_obj import Users
from helpers.pydb import mongo


class UserController(object):

    @staticmethod
    def authenticate(email, password):
        login_users = mongo.db.user.find_one({'email': email})

        if login_users:
            login_users['id'] = str(login_users['_id'])

            user_obj = Users(**login_users)

            return user_obj

    @staticmethod
    def identity(payload):
        user_id = payload['identity']
        user = mongo.db.user.find_one({'_id': ObjectId(user_id)})

        return user
