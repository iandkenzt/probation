from flask import Blueprint, request, jsonify, current_app
from flask_jwt import jwt_required, current_identity
from helpers import mongo
from werkzeug.local import LocalProxy

user = Blueprint('user', __name__)

_jwt = LocalProxy(lambda: current_app.extensions['jwt'])


@user.route('/user')
@jwt_required()
def index():
    current_id_users = current_identity['_id']
    info_user = mongo.db.user.find_one({'_id': current_id_users})

    all_user = mongo.db.user.find()
    print(all_user )

    return jsonify(name=info_user['name'], email=info_user['email'])


@user.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

        log_usr = mongo.db.user.find_one({'email': email})
        pwd_usr = log_usr['password']

        if log_usr:
            if pwd_usr == password:
                identity = _jwt.authentication_callback(email, password)
                encoded_jwt = _jwt.jwt_encode_callback(identity)

                return encoded_jwt

            if pwd_usr != password:
                return jsonify({'result': 'Password not match.'})

        return jsonify({'result': 'Email not match.'})

@user.route('/register', methods=['POST'])
def register():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')
        name = request.form.get('name')

        exsist_user = mongo.db.user.find_one({'email': email})

        if exsist_user is None:
            new_user = {"name": name, "password": password, "email": email}
            mongo.db.user.insert(new_user)

            return jsonify(status='200', data=new_user)

        else:
            return jsonify({'result': 'User already.'})

        return jsonify({'result': 'Please register again.'})
