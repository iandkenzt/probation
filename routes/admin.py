from flask import Blueprint, jsonify
from helpers import mongo

admin = Blueprint('admin', __name__)

@admin.route('/admins', methods=['GET'])
def index():
    user_json = []

    for user in list(mongo.db.user.find()):
        user['_id'] = str(user['_id'])
        user_json.append(user)

    return jsonify(status='200', user=user_json)